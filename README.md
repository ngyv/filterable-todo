This is a basic todo list app! Each todo item has a name, and a category. The displayed todo items can be filtered by category.

To see the app, clone it and then run:

```
npm start
```

Take a look at it, then complete the 3 tasks below. Submit a PR with your solution and with answers to the questions in Task 3.

## Task 1
The dropdown should filter the displayed list items by category, but it currently doesn't. Open `TodoList.js` and take a look at the `render` method. Add the code to filter the list of items and make the dropdown work correctly.

## Task 2
At the bottom of the todo list, add a form that enables you to add a new todo. You should use two inputs and a submit button, html approximately like this:

```
<form class="new-todo-form">
    <input class="new-todo-name" placeholder="todo name" />
    <input class="new-todo-category" placeholder="category" />
    <input type="submit" value="add todo" />
</form>
```

## Task 3
Answer these questions:

1. What are some edge cases that you would test for your "new todo" feature? 
2. What changes would you make if the todo list needed to handle a huge number of todo list items? How could you improve the performance of the category dropdown? Of the list rendering?
