import React from 'react';
import './CategoryFilter.css';

class CategoryFilter extends React.Component {
    onChange(e) {
        this.props.onChange(e.target.value);
    }

    render() {
        const options = this.props.options.map((option) => {
            return (
                <option value={option} key={option}>{option}</option>
            );
        });
        return (
            <select className="category-filter" onChange={(e) => {this.onChange(e)}} value={this.props.value} >
                {options}
            </select>
        );
    }
}

export default CategoryFilter;
