import React from 'react';
import './App.css';

import TodoList from './TodoList.js';
import CategoryFilter from './CategoryFilter';


class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [
                {
                    name: 'wash the floor',
                    category: 'home',
                },
                {
                    name: 'go to bank',
                    category: 'finances',
                }
            ],
            filter: '*',
        };
    }

    categoryChange(new_value) {
        this.setState({
            filter: new_value,
        });
    }

    calculateOptions() {
        let all_options = {};
        this.state.items.forEach((item) => {
            all_options[item.category] = 1;
        });

        let option_list = Object.keys(all_options);
        option_list.unshift('*');
        return option_list;
    }

    render() {
        return (
            <div className="App">
                <div className="App-banner">
                    <h1 className="App-title">Lumen5 Todo List</h1>
                    <CategoryFilter
                        onChange={(new_value) => this.categoryChange(new_value)}
                        options={this.calculateOptions()}
                        value={this.state.filter} />
                </div>
                <div className="App-body">
                    <TodoList items={this.state.items} filter={this.state.filter} />
                </div>
            </div>
        );
    }
}

export default App;
