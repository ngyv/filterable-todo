import React from 'react';
import './TodoList.css';

import TodoItem from './TodoItem.js';

class TodoList extends React.Component {
    render() {
        // TODO: filter the rendered items based on this.props.filter
        const items = this.props.items.map((item, index) => {
            return (
                <TodoItem item={item} key={index}/>
            );
        });

        return (
            <ol className="todo-list">
                {items}
            </ol>
        );
    }
}

export default TodoList;
