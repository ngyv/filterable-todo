import React from 'react';
import './TodoItem.css';

class TodoItem extends React.Component {
    render() {
        return (
            <li className="todo-item">
                <span className="todo-name">{this.props.item.name}</span>
                <span className="todo-category">{this.props.item.category}</span>
            </li>
        );
    }
}


export default TodoItem;
